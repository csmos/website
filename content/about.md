+++
title = "About"
date = "2020-10-22"
aliases = ["about-us","about-csmos","contact"]
[ author ]
  name = "csmos.org"
+++

csmOS is a GNU/Linux distribution based on the [Yocto
Project](https://www.yoctoproject.org/) for embedded Linux
systems. csmOS is delivered as a rolling-release model for multiple
architectures, providing the latest software available with the minimal
downstream changes.
